package com.nt.model;

import java.util.List;

public class UserRating1 {
	private List<Rating> abc;
	
	
	public UserRating1() {
		super();
	}

	public UserRating1(List<Rating> userRating1) {
		super();
		this.abc = userRating1;
	}

	public List<Rating> getUserRating() {
		return abc;
	}

	public void setUserRating(List<Rating> userRating2) {
		this.abc = userRating2;
	}
	
}
