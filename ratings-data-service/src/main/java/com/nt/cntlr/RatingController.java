package com.nt.cntlr;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nt.model.Rating;
import com.nt.model.UserRating1;

@RestController
@RequestMapping("/rating")
public class RatingController {

	@RequestMapping("/{movieId}")
	public Rating getRating(@PathVariable("movieId") String movieId) {
		return new Rating(movieId,5);
	}
	
	@RequestMapping("/user/{userId}")
	public UserRating1 getUserRating(@PathVariable("userId") String userId) {
		List<Rating> list = Arrays.asList(new Rating("Baghban",6),new Rating("Hero",7),new Rating("Boss",9));
		UserRating1 abc1 = new UserRating1();
		abc1.setUserRating(list);
		return abc1;
	}
}
