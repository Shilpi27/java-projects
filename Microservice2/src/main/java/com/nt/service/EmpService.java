package com.nt.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.dao.EmpRepo;
import com.nt.entity.Employee;

@Service
public class EmpService {

	@Autowired
	EmpRepo repo;
	public List<Employee> getAllEmp() {
		List<Employee> al= new ArrayList<Employee>();
		repo.findAll().forEach(item->al.add(item));
		return al;
	}
	
}
