package com.nt.cntlr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nt.entity.Employee;
import com.nt.service.EmpService;
import com.nt.utility.JsonHelper;

@RestController
public class Controller {

	@Autowired
	EmpService service;
	
	@GetMapping("/getAllEmp")
	public String getAll() {
		List<Employee> list = service.getAllEmp();
		return JsonHelper.toJsonArray(list).toString();
	}
}
