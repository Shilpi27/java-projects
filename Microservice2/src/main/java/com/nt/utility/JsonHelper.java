package com.nt.utility;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.nt.entity.Employee;

public class JsonHelper {
	public static JSONObject toJson(Employee e) {
		JSONObject obj = new JSONObject();
		obj.put("id", e.getId());
		obj.put("name", e.getName());
		obj.put("sal", e.getSal());
		return obj;
	}

	public static JSONArray toJsonArray(List<Employee> list) {
		JSONArray arr = new JSONArray();
		for (Employee e : list) {
			arr.add(toJson(e));
		}
		return arr;
	}
}
