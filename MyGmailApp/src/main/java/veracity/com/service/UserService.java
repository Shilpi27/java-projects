package veracity.com.service;

import java.sql.SQLException;

import veracity.com.dao.UserDao;
import veracity.com.entity.UserDetail;

public class UserService {
	UserDao uDao = new UserDao();

	public boolean validateLogin(String username, String password) throws SQLException {
		return uDao.validateLogin(username,password);
	}

	public String[] getMailMesgs(String loggedInUser,String MesgType) {
		return uDao.getMailMesgs(loggedInUser,MesgType);
	}

	public boolean registerUser(UserDetail user) {
		return uDao.registerUser(user);
	}

	public boolean ifUserExists(String username) {
		return uDao.ifUserExists(username);
	}
	
	public UserDetail getUserInfo(String loggedInUser) {
		return uDao.getUserInfo(loggedInUser);
	}

	public boolean sendMesg(String sendTo, String sendFrom, String message) {
		return uDao.sendMesg( sendTo, sendFrom, message);
		
	}	

	
}
