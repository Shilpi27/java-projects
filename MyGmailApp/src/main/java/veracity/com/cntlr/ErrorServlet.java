package veracity.com.cntlr;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ErrorServlet extends HttpServlet{
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		Exception e = null;
	    e = (Exception) req.getAttribute("errorMesg");
		String   errorDesc = (String) req.getAttribute("errorDesc");
		System.out.println("errorDesc :" + errorDesc );
		PrintWriter pw = res.getWriter();
		//pw.write("<h2 style='color:red'>Error Occurred</h2>");
		if (e != null)
			pw.write("<p style='color:red'>" + e.toString()+ "</p>");
		pw.write("<p style='color:red'>" + errorDesc + "</p>");
	}
}