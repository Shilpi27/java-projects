package veracity.com.cntlr;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import veracity.com.entity.UserDetail;
import veracity.com.service.UserService;

public class InboxController extends HttpServlet{
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String loggedInUser = null;
		/*Cookie[] ck = req.getCookies();
		if (ck != null) {
			for (Cookie c:ck) {
				String key = c.getName();
				if (key.equals("User")) {
					loggedInUser = c.getValue();
				}
			}
		}*/
		HttpSession session = req.getSession(false); //false becoz we dont want to create new session,only fetch data stored in created session
		if (session == null) {
			req.setAttribute("error", "plz login first");
			RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
			rd.include(req,res);
		}
		else {
			//go to service->dao
			loggedInUser = (String) session.getAttribute("User");
			UserService uService = new UserService();
			String[] inboxMesgs  = uService.getMailMesgs(loggedInUser,"Inbox");
			UserDetail uDetail   = uService.getUserInfo(loggedInUser);
			String fullName = uDetail.getFirstName() + " " + uDetail.getLastName();
			req.setAttribute("Mesgs", inboxMesgs);
			req.setAttribute("User", loggedInUser);
			req.setAttribute("MesgType", "Inbox");
			req.setAttribute("FullName", fullName);
			RequestDispatcher rd = req.getRequestDispatcher("DisplayMesg.jsp");
			rd.forward(req,res);			
			/*res.setContentType("text/html");
			PrintWriter pw = res.getWriter();
			pw.write("<h1>welcome to inbox " + loggedInUser + " </h1>");*/
		}
	}
}
