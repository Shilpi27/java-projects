package veracity.com.cntlr;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import veracity.com.entity.UserDetail;
import veracity.com.service.UserService;


public class LoginController extends HttpServlet{
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		UserService uService = new UserService();
		try {
			boolean isValidUser = uService.validateLogin( username,password );
			if (isValidUser) {
				UserDetail uDetail   = uService.getUserInfo(username);
				String fullName = uDetail.getFirstName() + " " + uDetail.getLastName();
				req.setAttribute("user", username);
				req.setAttribute("FullName", fullName);
				/*Cookie ck = new Cookie("User",username);
				ck.setMaxAge(15);
				res.addCookie(ck);      */     //using cookie
				HttpSession session = req.getSession(); //using httpSession
				session.setAttribute("User",username);
				RequestDispatcher rd = req.getRequestDispatcher("Home.jsp");
				rd.forward(req, res);
			}
			else {
			/*	res.setContentType("text/html");
				PrintWriter pw = res.getWriter();
				pw.write("<h2>Invalid Credential..try again</h2>");	*/
				//Above code works 
				req.setAttribute("error", "Invalid Credentials ..try again"); //alternative way
				RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
				//rd.include(req, res); //when wants to send output from source also using printwriter code above
				rd.forward(req, res);
			}
		} catch (Exception e) {
			req.setAttribute("errorMesg", e);
			RequestDispatcher rd = req.getRequestDispatcher("/error");
			rd.forward(req, res);
		 }
	}
}