package veracity.com.cntlr;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import veracity.com.entity.UserDetail;
import veracity.com.service.UserService;

public class RegisterController extends HttpServlet{
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException  {
		String firstName = req.getParameter("firstName");
		String lastName  = req.getParameter("lastName");
		String username  = req.getParameter("username");
		String password  = req.getParameter("password");
		String confirm   = req.getParameter("confirm");
		System.out.println("RegisterController data :: " + firstName + " : " + lastName + " : " + username + " : " + password + ": " + confirm );
		if (password.equals(confirm)) {
			UserDetail user = new UserDetail(firstName,lastName,username,password);
			UserService uService = new UserService();
			boolean result = uService.ifUserExists(username);
			System.out.println("result :" + result);
			if (result == true) {
				req.setAttribute("errorDesc", "User already registered, please signin");
				RequestDispatcher rd = req.getRequestDispatcher("/error");
				rd.forward(req,res);
			}
			else
			{
				boolean reg = uService.registerUser(user);
				if (reg == true) {
						res.setContentType("text/html");
						PrintWriter pw = res.getWriter();
						pw.write("<h2>Registered Successfully</h2>");
				}
				else {
					req.setAttribute("errorDesc", "Sorry, Cannot register..Plz try again");
					RequestDispatcher rd = req.getRequestDispatcher("/error");
					rd.forward(req,res);
				}
			}
		}
		else
		{
			req.setAttribute("errorDesc", "Password does not match");
			RequestDispatcher rd = req.getRequestDispatcher("/error");
			rd.forward(req,res);
		}
	}
}
