package veracity.com.cntlr;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogoutController extends HttpServlet{
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		/*Cookie ck = new Cookie("User",null);
		ck.setMaxAge(0);
		res.addCookie(ck);*/
		HttpSession session = req.getSession(false);
		if (session != null) {
			session.invalidate();
			req.setAttribute("error", "Logout Successfully");
			RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
			rd.forward(req, res);
		}
	}
}
