package veracity.com.cntlr;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import veracity.com.entity.UserDetail;
import veracity.com.service.UserService;

public class ComposeController extends HttpServlet{
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String sendTo = req.getParameter("username");
		String message = req.getParameter("message");
		UserService uService = new UserService();
		try {
			boolean isValidUser = uService.ifUserExists( sendTo );
			if (isValidUser) {
				System.out.println("Valid User");
				HttpSession session = req.getSession(); //using httpSession
				String sendFrom = (String) session.getAttribute("User");
				System.out.println("sendFrom :" +sendFrom);
				req.setAttribute("user", sendFrom);
				boolean sendResult = uService.sendMesg(sendTo,sendFrom,message);
				if (sendResult)
					req.setAttribute("result", "Sent Successfully");
				else
					req.setAttribute("result", "Error in Sending");
				RequestDispatcher rd = req.getRequestDispatcher("Home.jsp");
				rd.forward(req, res);
			}
			else {
				req.setAttribute("error", "User does not exist"); //alternative way
				RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
				rd.forward(req, res);
			}
		} catch (Exception e) {
			req.setAttribute("errorMesg", e);
			RequestDispatcher rd = req.getRequestDispatcher("/error");
			rd.forward(req, res);
		 }
	}
}