package veracity.com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import veracity.com.entity.UserDetail;

public class UserDao {

	public boolean validateLogin(String username, String password) throws SQLException {
		Connection conn = null;
		try 
		{
			conn = ConnectionFactory.getDaoConnection();
			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("select * from login where username=? and password=?");
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Record present in login :"  + rs.getString(1) + " : "+ rs.getString(2));
				return true;
			}
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		/*finally{
			conn.close();
		}*/
		return false;
	}

	public String[] getMailMesgs(String username,String MesgType) {
		Connection conn = null;
		String[] mesgs = null;
		try 
		{
			conn = ConnectionFactory.getDaoConnection();
			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("select * from mail_mesg where username=? and messageType=?");
			ps.setString(1, username);
			ps.setString(2, MesgType);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Record present in mail_mesg :"  + rs.getString(1) + " : "+ rs.getString(2) + " : "+ rs.getString(3));
				mesgs = rs.getString(2).split(",");
				return mesgs;
			}		
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return mesgs;
	}

	public boolean ifUserExists(String username) {
		System.out.println("In ifUserExists");
		Connection conn = null;
		try 
		{
			conn = ConnectionFactory.getDaoConnection();
			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("select * from login where username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Record present in login :"  + rs.getString(1) + " : "+ rs.getString(2));
				return true;
			}
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return false;
	}

	public boolean registerUser(UserDetail user) {
		System.out.println("In registerUser");
		Connection conn = null;
		try 
		{
			conn = ConnectionFactory.getDaoConnection();
			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("insert into login values(?,?,?,?)");
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getFirstName());
			ps.setString(4, user.getLastName());
			int result = ps.executeUpdate();
			if (result > 0) {
				return true;
			}
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return false;
	}

	public UserDetail getUserInfo(String username) {
		Connection conn = null;
		try 
		{
			conn = ConnectionFactory.getDaoConnection();
			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("select * from login where username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Record present in login :"  + rs.getString(1) + " : "+ rs.getString(2));
				return new UserDetail(rs.getString(3),rs.getString(4),rs.getString(1),rs.getString(2));
			}
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return null;
	}

	public boolean sendMesg(String sendTo, String sendFrom, String message) {
		Connection conn = null;
		try 
		{
			conn = ConnectionFactory.getDaoConnection();
			System.out.println("Connection created " + conn);
			
			PreparedStatement ps = conn.prepareStatement("select * from mail_mesg where username=? and messageType=?");
			ps.setString(1, sendTo);
			ps.setString(2, "Inbox");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				System.out.println("Record found in mail_mesg " + sendTo + " sendFrom " + sendFrom + " message " +message);
				String newMesg = rs.getString(2) + "," + message;
				PreparedStatement ps1 = conn.prepareStatement("update mail_mesg set message=? where username=? and messageType=?");
				ps1.setString(1, newMesg);
				ps1.setString(2, sendTo);
				ps1.setString(3, "Inbox");
				int result = ps1.executeUpdate();
				if (result > 0) {
					return true;
				}
			}
			else {
				System.out.println("Record not found so inserting");
				System.out.println("Record not found in mail_mesg " + sendTo + " sendFrom " + sendFrom + " message " +message);
				String newMesg =  message;
				PreparedStatement ps2 = conn.prepareStatement("insert into mail_mesg values(?,?,?)");
				ps2.setString(1, sendTo);
				ps2.setString(2, newMesg);
				ps2.setString(3, "Inbox");
				int result = ps2.executeUpdate();
				if (result > 0) {
					return true;
				}
			}
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return false;
	}
}
