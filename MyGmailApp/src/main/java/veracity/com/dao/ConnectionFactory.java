package veracity.com.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {
	static Connection conn = null;
	static {
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
		    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}		
	}
	public static Connection getDaoConnection() {
		return conn;
	}
}