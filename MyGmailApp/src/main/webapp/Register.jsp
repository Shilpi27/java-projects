<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="background-color:DodgerBlue;">
    <%
        String passwordCondition = "Use 8 or more characters with a mix of letters, numbers & symbols";
        String usernameCondition = "You can use letters,numbers & periods";
        pageContext.setAttribute("Password", passwordCondition);
        pageContext.setAttribute("Username", usernameCondition);
     %>
   	<div align="center" style="border:1px solid blue">
   		<h2>Create your Google Account</h2>
		<form action="register">
			<input type="text" name="firstName" placeholder="First name">&emsp;
			<input type="text" name="lastName" placeholder="Last name"><br><br>
			<input type="text" name="username" placeholder="username   @gmail.com">
			<p>${Username}</p>
			<input type="text" name="password" placeholder="Password">&emsp;
			<input type="text" name="confirm" placeholder="Confirm">
			<p>${Password}</p>
			<div align="left" >
				<a href="Login.jsp">Sign in instead</a>
			</div>
			<div align="right" >
				<input type="submit" value="submit"><br>
			</div>
		</form>
	</div>	
</body>
</html>