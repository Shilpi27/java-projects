<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>table, th, td {border: 1px solid black;}</style>
</head>
<body style="background-color:DodgerBlue;">
	<div align="right" style="border:1px black">
		${FullName}<br><br>	
		${User}
	</div>	
	<table style="background-color:white;" >
		<tr style='color:DodgerBlue'>
			<th> </th>
			<th>${MesgType}</th>
		</tr>
	  <c:forEach items="${Mesgs}" var="Mesg">
		<tr style='color:black'>
			<td> * </td>
			<td> ${Mesg} </td>
		</tr>
	   </c:forEach>
	</table>
</body>
</html>