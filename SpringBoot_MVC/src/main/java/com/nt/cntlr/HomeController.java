package com.nt.cntlr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nt.entity.Employee;
import com.nt.service.EmpService;

@Controller
public class HomeController {
	@Autowired
	EmpService service;

	@RequestMapping(value = "/registerPage", method = RequestMethod.GET)
	public String registerPage() {
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@ModelAttribute Employee e, Model model) {
		boolean isAdded = service.add(e);
		if (isAdded)
			model.addAttribute("msg", "Successful");
		else
			model.addAttribute("msg", "not registerd");
		return "display";
	}
}
