package com.nt.cntlr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nt.entity.Employee;
import com.nt.service.EmpService;

@RestController
public class Controller {

	@Autowired
	EmpService service;
	
	@GetMapping("/getAll")
	public String getAll() {
		return service.getAllEmp();
	}
}

