package com.nt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmpService {

	@Autowired
	RestTemplate template;
	
	public String getAllEmp() {
		ResponseEntity<String> res = template.getForEntity("http://localhost:7272/getAllEmp", String.class);
		return res.getBody();
	}

}
