package com.nt.cntlr;

public class RandomPasswordGen {
	public static String randomPass() {
		StringBuilder sb = new StringBuilder();
		int n = 8; // how many characters in password
		String set ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*()_+-=[]|,./?><"; // characters to choose from

		for (int i= 0; i < n; i++) {
		    int k = (int)(Math.random()*100); // random number between 0 and set.length()-1 inklusive
		    sb.append(set.charAt(k));
		}
		System.out.println("Random Password :" + sb.toString());
		return sb.toString();
	}
}
