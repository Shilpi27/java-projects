package com.nt.cntlr;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nt.model.CatalogItem;
import com.nt.model.Movie;
import com.nt.model.UserRating;

@RestController
@RequestMapping("/Catalog")
public class MovieCatalogController {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	private DiscoveryClient discoveryClient;
	
	//@Autowired
	//WebClient.Builder webClientBuilder; //= WebClient.builder();
	
	@RequestMapping("/{userId}")
	public List<CatalogItem> getCatalog(@PathVariable("userId") String userId){
			
		//List<Rating> ratingList = Arrays.asList(new Rating("DDLJ",10),new Rating("KKHH",11));
		//call API to get rating details of user
		//movie-rating-service is application name of rating service API at port 8083
		//UserRating userRating = restTemplate.getForObject("http://localhost:8083/rating/user/"+userId, UserRating.class );
		UserRating userRating = restTemplate.getForObject("http://movie-rating-service/rating/user/"+userId, UserRating.class );
		return userRating.getUserRating().stream().map(rating -> {
						  //   Movie movie = restTemplate.getForObject("http://localhost:8082/movies/"+rating.getMovieId(), Movie.class);
			 				Movie movie = restTemplate.getForObject("http://movie-info-service/movies/"+rating.getMovieId(), Movie.class);
							return new CatalogItem(movie.getName(),"desc",rating.getRating());
							}).collect(Collectors.toList());
		
		/*List<CatalogItem> list = new ArrayList<CatalogItem>();
		list.add(new CatalogItem(movie.getName(),"fight",4));
		return list;*/
	}

}

/*Movie movie = webClientBuilder.build()  //for asynchrous call
.get()
.uri("http://localhost:8082/movies/" + rating.getMovieId())
.retrieve()
.bodyToMono(Movie.class)
.block();*/