<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/myStyles.css">
</head>
<body>
<jsp:include page="PatientDetails.jsp"></jsp:include><br><br>
  <fieldset>
    <legend>Patients Record :</legend>
<table>
	<tr>
		<th>Id</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Gender</th>
		<th>DOB</th>
		<th>Address</th>
		<th>Telephone No</th>
		<th>Admitted Date</th>
		<th>Dr. Code</th>
	</tr>
	 <c:forEach items="${PatientList}" var="patient">
	<tr>
		<td>${patient.id}</td>
		<td>${patient.firstName}</td>
		<td>${patient.lastName}</td>
		<td>${patient.gender}</td>
		<td>${patient.dob}</td>
		<td>${patient.address}</td>
		<td>${patient.telephone}</td>
		<td>${patient.admitDate}</td>
		<td>${patient.drcode}</td>
	</tr>
	   </c:forEach>
</table>
</fieldset>
</body>
</html>