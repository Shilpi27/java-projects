<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/myStyles.css">
</head>
<body>
	<jsp:include page="PatientDetails.jsp"></jsp:include><br><br>
<p style="color:white;">Please fill below Patient details :</p>
<form action="add" method="post">
	<table>
	<tr>
		 <th>First Name :</th>
		 <td><input type="text" name="firstName"></td> 
	</tr>
	<tr>
		 <th>Last Name :</th>
		 <td><input type="text" name="lastName"></td> 
	</tr>
	<tr>
		 <th>Gender :</th>
		 <td><input type="radio" name="gender" value="male" checked>Male<br>
		 	 <input type="radio" name="gender" value="female"> Female<br>
		 	 <input type="radio" name="gender" value="other"> Other</td> 	 	 
	</tr>
	<tr>
		 <th>D.O.B :</th>
		 <td><input type="date" name="dob"></td> 
	</tr>
	<tr>
		<th>Address :</th>
		<td><input type="text" name="address"></td>
	</tr>
	 <tr>
		 <th>Telephone No :</th>
		 <td><input type="tel" name="telephone"></td> 
	</tr>
	<tr>
		<th>Admitted Date :</th>
		<td><input type="date" name="admitDate"></td>
	</tr>
	<!--<tr>
		<<th>>Doctor's Code :</th>
		<td><input type="text" name="drcode"><a href="Doctor">Check Available Doctor's</a></td>
	</tr>-->
	<tr>
		<th></th>
		<td><input type="submit" value="submit"><input type="reset"></td>
	</tr>
	</table>
</form>
</body>
</html>