package com.nt.cntlr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nt.entity.Patient;
import com.nt.service.LoginService;
import com.nt.service.PatientService;

@Controller
public class HospitalController {
	
	//private static final Patient NULL = null;
	@Autowired
	PatientService service;
	
	@Autowired
	LoginService   lservice;
	
	@RequestMapping("/loginForm")
	public String loginForm() {
		return "display";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String validateLogin(@RequestParam("username") String username,@RequestParam("password") String Password,Model model) {
		System.out.println("In HospitalController " + username + " "+Password );
		boolean isValidUser = lservice.validateLogin(username,Password);
		if( isValidUser) {  
			return "Home";
		}
		else {
			model.addAttribute("msg","Invalid username and password..try again");
			return "login";
		}
	}
	
	@RequestMapping("/PatientDetails")
	public String patientForm() {
		return "PatientDetails";
	}
	
	@RequestMapping("/addForm")
	public String addForm() {
		return "addPatient";
	}

	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String addPatient(@ModelAttribute("pat") Patient p,Model model) {
		System.out.println("In Home : " +p.getId() + p.getFirstName()+p.getLastName());
		boolean isAdded = service.addPatient(p);
		if (isAdded)
			model.addAttribute("msg","Registered successfully");
		else
			model.addAttribute("msg","Sorry cannot register");
		return "display";
	}

	
	@RequestMapping("/removeForm")
	public String deleteForm() {
		return "removePatient";
	}

	@RequestMapping(value="/remove",method=RequestMethod.GET)
	public String removePatient(@RequestParam("id") int id,Model model) {
		boolean isRemoved = service.removePatientById(id);
		if (isRemoved)
			model.addAttribute("msg","Removed successfully");
		else
			model.addAttribute("msg","Sorry cannot remove");
		return "display";
	}
	
	@RequestMapping("/UpdateForm")
	public String updateForm() {
		return "updatePatient";
	}

	@RequestMapping(value="/update",method=RequestMethod.GET)
	public String updatePatient(@ModelAttribute("p") Patient p,Model model) {
		boolean isUpdated = service.updatePatientDetails(p);
		if (isUpdated)
			model.addAttribute("msg","Updated successfully");
		else
			model.addAttribute("msg","Sorry cannot update");
		return "display";
	}
	
	@RequestMapping("/SelectForm")
	public String selectForm() {
		return "selectPatient";
	}

	//@RequestMapping(value="/select",method=RequestMethod.GET) //for GET method no need to write whole parameter as GET is default
	@RequestMapping("/select")
	public String selectPatient(@RequestParam("id") int id,Model model) {
		Patient p = service.selectPatientById(id);
		if( p == null) {  
			System.out.println("Value of p is null ");
			model.addAttribute("msg","Sorry, No record present for given id");
			return "display";
		}
		else {
			System.out.println("Value of p " + p.toString());
			model.addAttribute("patient",p);
			return "displayRecord";
		}
	}
	
	@RequestMapping("/SelectAll")
	public String selectAllPatient(Model model) {
		List<Patient> al = service.selectAllPatient();
		if (al.isEmpty() || al == null) {
			model.addAttribute("msg","No patient record present ");
			return "display";
		}
		else {
			model.addAttribute("PatientList",al);
			return "displayAllPatient";
		}
	}
}
