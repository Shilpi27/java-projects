package com.nt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nt.dao.LoginDao;

@Component
public class LoginService {

	@Autowired
	LoginDao dao;
	
	public boolean validateLogin(String username, String password) {
		// TODO Auto-generated method stub
		return dao.validateLogin( username,  password);
	}
	
}
