package com.nt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nt.dao.PatientDao;
import com.nt.entity.Patient;

@Component
public class PatientService {

	@Autowired
	PatientDao dao;
	
	public boolean addPatient(Patient p) {
		// TODO Auto-generated method stub
		return dao.addPatient(p);
	}

	public boolean removePatientById(int id) {
		// TODO Auto-generated method stub
		return dao.removePatientById(id);
	}

	public boolean updatePatientDetails(Patient p) {
		return dao.updatePatientDetails(p);
	}

	public Patient selectPatientById(int id) {
		// TODO Auto-generated method stub
		return dao.selectPatientById(id);
	}

	public List<Patient> selectAllPatient() {
		// TODO Auto-generated method stub
		return dao.selectAllPatient();
	}

}
