package com.nt.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.nt.entity.Patient;

@Component
public class PatientDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	/*public boolean addPatient(Patient p) {
		Object[] args= {p.getName(),p.getAddress(),p.getAdmittedDate(),p.getDoctorName(),p.getRoomNo()};
		int result = jdbcTemplate.update("insert into patient (pname,paddress,padmitDate,pdoctorName,proomNo) values(?,?,?,?,?)",args);
		if (result == 1)
			return true;
		return false;
	}*/
	public boolean addPatient(Patient p) {
		Object[] args= {p.getFirstName(),p.getLastName(),p.getGender(),p.getDob(),p.getAddress(),p.getTelephone(),p.getAdmitDate()};
		int result = jdbcTemplate.update("insert into sch_patient (firstName,lastName,gender,dob,address,telphone,admitDate) values(?,?,?,?,?,?,?)",args);
		if (result == 1)
			return true;
		return false;
	}
	

	public boolean updatePatientDetails(Patient p) {
		Object[] args= {p.getTelephone(),p.getAddress(),p.getId()};
		int result = jdbcTemplate.update("update sch_patient set telphone=?,address=? where id=?",args);
		if (result == 1)
			return true;
		return false;
	}
	
	public boolean removePatientById(int id) {
		Object[] args= {id};
		int result = jdbcTemplate.update("delete from sch_patient where id=?",args);
		if (result == 1)
			return true;
		return false;
	}

	public Patient selectPatientById(int id) {
		
		Object[] args= {id};
		/*Patient p = jdbcTemplate.queryForObject("select * from patient where pid=?",args,new RowMapper<Patient>() {
			public Patient mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new Patient(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
			}});*/    // 3rd arg of queryForObject is anonymous class object which implements RowMapper and override mapRow func
		
	    Patient p = jdbcTemplate.queryForObject("select * from sch_patient where id=?",args,new PatientMapper());
	    return p;
	}

	public List<Patient> selectAllPatient() {
		List<Patient> p = jdbcTemplate.query("select * from sch_patient",new PatientMapper());
		return p;
	}
}
