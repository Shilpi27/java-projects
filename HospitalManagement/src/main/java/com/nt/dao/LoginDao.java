package com.nt.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.nt.entity.Login;

@Component
public class LoginDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public boolean validateLogin(String username, String password) {
		// TODO Auto-generated method stub
		Object args[] = {username,password};
		System.out.println("In LoginDao " + username + " " + password);
		Login l = jdbcTemplate.queryForObject("select * from sch_login where username=? and password=?",args,new RowMapper<Login>(){

			public Login mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				return new Login(rs.getString(1),rs.getString(2));
			}});
		if (l == null)
			return false;
		else
			return true;
	}
	
}
