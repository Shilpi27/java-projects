package com.nt.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.nt.entity.Login;

public class LoginMapper implements RowMapper<Login>{

	public Login mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Login(rs.getString(1),rs.getString(2));
	}
}