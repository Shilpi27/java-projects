package com.nt.entity;

import org.springframework.stereotype.Component;

@Component
public class Patient {
	/*private int id;
	private String name;
	private String address;
	private String admittedDate;
	private String doctorName;
	private String roomNo;

	public Patient() {
	}

	public Patient(int id, String name, String address, String admittedDate, String doctorName, String roomNo) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.admittedDate = admittedDate;
		this.doctorName = doctorName;
		this.roomNo = roomNo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAdmittedDate() {
		return admittedDate;
	}

	public void setAdmittedDate(String admittedDate) {
		this.admittedDate = admittedDate;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}*/
	private int id;
	private String firstName;
	private String lastName;
	private String gender;
	private String dob;
	private String address;
	private String telephone;
	private String admitDate;
	private int drcode;

	public Patient() {
	}

	public Patient(int id, String firstName, String lastName, String gender, String dob, String address, String telphone,
			String admitDate, int drcode) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dob = dob;
		this.address = address;
		this.telephone = telphone;
		this.admitDate = admitDate;
		this.drcode = drcode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {
		this.admitDate = admitDate;
	}

	public int getDrcode() {
		return drcode;
	}

	public void setDrcode(int drcode) {
		this.drcode = drcode;
	}

}
