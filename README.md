# README #

This README would normally document brief information of projects created.

### What is this repository for? ###

* Java Projects Created using Core Java, J2EE, Servlet ,JSP, Web Services, Spring ,Spring Boot ,Microservices ,JDBC ,HTML, CSS.
* RDBMS-MySQL, Apache Tomcat ,Maven ,Eclipse .
* Java - 1.8

### Java Projects ###
* Company Project :
  1)Create using Core Java ,Servlet ,JDBC, HTML, MySQL ,Apache Tomcat.
  2)Can perform all CRUD operation for a company.
  3)Register a Company information, Read company details, Update a company details, Delete company information and Read all company records.
  4)After Registration ,default password is generated and sent to Registered user email id .
  5)Password update through the link shared in email.

* Gmail_Spring :
  1) Create using Core Java ,Spring ,JSP, JDBCTemplate, HTML,CSS, MySQL ,Apache Tomcat.
  2) User can login ,logout and view email messages .
  3) Login validation from Database.
  4) Session to store user details for subsequent request.

* Hospital Managemnt :
  1) Create using Core Java ,Spring ,JSP, JDBCTemplate, HTML,CSS, MySQL ,Apache Tomcat.
  2) SignIn and SignUp form for Hospital Admin team.
  3) Adding, Reading, Updating and Removing Patient Details.
  4) View all patients registered.

* Mail Sender:
  1)Create using Core Java ,Servlet ,Apache Tomcat.
  2)Sending Mail to particular Email Account with custom message.

* MyGmailApp :
  1)Create using Core Java ,Servlet ,JSP, JDBC, RequestDispatcher, MySQL ,Apache Tomcat.
  2)SignIn and SignUp form.
  3)Validation after SignIn to authenticate the user .
  4)HttpSession and Cookie to store user details after successful authentication.
  5)Displaying Inbox,Draft and sent messages by fetching from database using jstl and JSP expression language.
  6)Logout tab and removing the session.


* Microservice1 and Microservice2:
  1)Create using Core Java ,Spring Boot ,JSP, Hibernate, JDBCTemplate, HTML, CSS, MySQL ,Apache Tomcat.
  2)Microservice1 communicates with Microservice2.
  3)Request Comes to Microservice1 and after validation trasfers the request to Microservice2 to read data from database.
  4)Response is forwarded from Microservice2 to Microservice1 to send it back to client.

* SpringBoot_MVC :
  1)Create using Core Java ,Spring Boot ,JSP, Hibernate, JDBCTemplate, HTML, CSS, MySQL ,Apache Tomcat.
  2)Application containing Model ,View and Controller.
  3)Display the JSP pages.

* JerseyApp:
  1)Create using Core Java ,REST API ,JDBC, HTML, MySQL ,Apache Tomcat.
  2)Employee Management for all CRUD operation.
  3)View Employee details based on Client Input either by Id,Name,etc.
  4)Used PathParam and QueryParam for taking input values.
  5)Register ,Update ,View and Delete Specific or all employees.
  6)Used Postman to test the URL patterns.
  7)Sending JSON object and JSON Array as Response.

* Boot_Rest:
  1)Create using Core Java ,REST API ,Spring Boot ,JWT ,Hibernate ,JDBC, HTML, MySQL ,Apache Tomcat.
  2)Employee Management for all CRUD operation using Spring Boot Framework.
  3)User Authentication by validation and Token System of JWT.
  4)For Subsequent request from client verification of JWT token sent by client.
  5)Used Interceptor feature to create and verify tokens for subsequent request.
  6)Used Secret Message and algorithm(HMAC256) at server side which is common for token creation and verification.  
