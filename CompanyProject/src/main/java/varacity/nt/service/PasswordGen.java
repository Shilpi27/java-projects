package varacity.nt.service;

public class PasswordGen {
	public static String randomPass() {
		StringBuilder sb = new StringBuilder();
		int n = 8; //characters in password
		String set ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*()_+-=[]|,./?><"; // characters to choose from

		for (int i= 0; i < n; i++) {
		    int k = (int) (Math.random() * ((set.length() -1) - 0)) + 0; // random number between 0 and set.length()-1 
		    sb.append(set.charAt(k));
		}
		System.out.println("Random Password :" + sb.toString());
		return sb.toString();
	}
	public static void main(String[] args) {
		System.out.println(PasswordGen.randomPass());
	}
}
