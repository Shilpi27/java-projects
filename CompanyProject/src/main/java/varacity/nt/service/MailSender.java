package varacity.nt.service;

import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {
  public static void sendMail(String to,String from,String password,String mailMessage ) throws Exception {
	  final String user= from;
	  final String pass= password;
	  String mailhost = "smtp.gmail.com";

	  Properties props= System.getProperties();
	  props.put("mail.smtp.auth", "true"); 
	  props.put("mail.smtp.user", user);
	  props.put("mail.smtp.password", pass);
	  props.put("mail.smtp.starttls.enable", "true");
	  props.put("mail.smtp.host", mailhost);
	  props.put("mail.smtp.port", "587");
      
	  Session session= Session.getInstance(props,  new javax.mail.Authenticator()  {

	      @Override
	      protected PasswordAuthentication getPasswordAuthentication(){ 
	          return new PasswordAuthentication(user,pass);   
	      }
	  }); 
	  MimeMessage message= new MimeMessage(session);
	  message.setFrom(new InternetAddress(from));
	  message.setRecipient(RecipientType.TO, new InternetAddress(to));
	  message.setSubject("Password Reset E-mail ");
	  message.setText(mailMessage);
	  Transport.send(message);
	  System.out.println("Sent!");
  }
}
