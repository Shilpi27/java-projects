package veracity.nt.cntlr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import varacity.nt.service.MailSender;
import varacity.nt.service.PasswordGen;
import veracity.nt.dao.CompanyDao;
import veracity.nt.dao.LoginDao;
import veracity.nt.entity.Company;

public class RegisterController extends HttpServlet{
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException  {
	//	System.out.println("Inside service of RegisterController");
		String name= req.getParameter("company name");
		String location = req.getParameter("location");
		String adminName = req.getParameter("admin name");
		String adminMail = req.getParameter("admin mail");
		System.out.println("name " + name + " location " +location+ " adminName " + adminName + " adminMail " +adminMail);
		CompanyDao cod = new CompanyDao();
		Company cObj = new Company(name,location,adminName,adminMail);
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		try {
			cod.add(cObj);
			LoginDao ld1 = new LoginDao();
			String passwordGenerated = PasswordGen.randomPass();
			System.out.println("Random Password Generated :" + passwordGenerated);
			ld1.add(adminMail, passwordGenerated);  //Generating Random Password
			//Taking Input from user--Senders Email address and Senders Password
			Scanner sc = new Scanner(System.in);
			System.out.println("Please enter sender's email :");
			String senderEmail    = sc.next();
			System.out.println("Please enter sender's password :");
			String senderPassword = sc.next();        //Senders Password
			//sc.close();
			pw.write("<h2>Registered successfully</h2>");
			pw.append("<h2>Email has been sent with your username/password..Please click the link to reset the password</h2>");
			String mailMessage = "Username : " + adminMail + "\nPassword :" + passwordGenerated + "\nClick the link below to reset your password:\n" + "http://localhost:8086/CompanyProject/Password.html";
			MailSender.sendMail(adminMail,senderEmail,senderPassword,mailMessage);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			pw.write("<h2>Could Not register..try again</h2>");
			e.printStackTrace();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			pw.write("<h2>Could Not register..try again</h2>");
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			pw.close();
		}
	}
}
