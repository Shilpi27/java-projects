package veracity.nt.cntlr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import veracity.nt.dao.CompanyDao;

public class ViewCompanyController extends HttpServlet {
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		int id = Integer.parseInt(req.getParameter("id"));
		System.out.println("Input id " + id);
		CompanyDao cdObj = new CompanyDao();
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		try {
			String companyDetails = cdObj.viewCompanyDetails(id);
			//System.out.println("companyDetails " +companyDetails);
			if (companyDetails.equals("null") == false) {
				String tableBorder = "<html style='background-color:powderblue;'><style>table, th, td {border: 1px solid black;}</style>";
				pw.write(tableBorder);
				pw.append( companyDetails);
				//pw.append("</html>");
			}
			else
				pw.write("Could not find company details");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			pw.write("Could not find company details");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			pw.write("Could not find company details");
			e.printStackTrace();
		}
		finally {
			pw.close();
		}
	}

}
