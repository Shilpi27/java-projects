package veracity.nt.cntlr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import veracity.nt.dao.CompanyDao;
import veracity.nt.dao.LoginDao;

public class RemoveController extends HttpServlet{
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException  {
		int id= Integer.parseInt(req.getParameter("id"));
		System.out.println(" id " +id);
		CompanyDao cod = new CompanyDao();
		LoginDao ld = new LoginDao();
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		try {
			ld.removeUser(cod.getAdminMail(id));  //remove users entry from Login table
			if (cod.remove(id))                           //remove entry from Company table
				pw.write("<h2>Removed successfully</h2>");
			else
				pw.write("<h2>Id does not exist</h2>");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			pw.write("<h2>Could not remove..try again</h2>");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			pw.write("<h2>Could not remove..try again</h2>");
			e.printStackTrace();
		}
		finally {
			pw.close();		
		}
	}
}
