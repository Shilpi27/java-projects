package veracity.nt.cntlr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import veracity.nt.dao.CompanyDao;

public class ViewAllController extends HttpServlet{
	protected void service (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		System.out.println("Selecting all records from Company table");
		CompanyDao cdObj = new CompanyDao();
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		try {
			String tableBorder = "<html style='background-color:powderblue;'><style>table, th, td {border: 1px solid black;}</style>";
			//String allCompany = cdObj.viewAllCompany();
			pw.write(tableBorder);			
		    pw.append( cdObj.viewAllCompany());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			pw.write("Could not find company details");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			pw.write("Could not find company details");
			e.printStackTrace();
		}
		finally {
			pw.close();
		}
	}

}
