package veracity.nt.cntlr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import veracity.nt.dao.LoginDao;

public class PasswordController extends HttpServlet{
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException  {
		String username    = req.getParameter("username");
		String oldPassword = req.getParameter("Old Password");
		String newPassword = req.getParameter("New Password");
		String confirmPassword = req.getParameter("Confirm Password");
		System.out.println( "username " + username + " old " +oldPassword+ " new " + newPassword + " confirm " +confirmPassword);
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		if (newPassword.equals(confirmPassword) == false)
		{
			System.out.println("New and confirm password are not same");
			pw.write("<h2>New and Confirm Password does not match</h2>");
		}
		else
		{
			LoginDao ld =new LoginDao();
			try {
				if (ld.validateLogin(username, oldPassword)) {
					ld.updatePassword(username, newPassword);
					System.out.println("Password Updated successfully");
					pw.write("<h2>Password Updated successfully</h2>");
				}
				else
				{
					System.out.println("No entry for username and password.");
					pw.write("<h2>No entry for given username and password.</h2>");					
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				pw.close();
			}
		}
	}
}
