package veracity.nt.cntlr;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import veracity.nt.dao.CompanyDao;
import veracity.nt.dao.LoginDao;

public class UpdateController extends HttpServlet{
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException  {
		int id= Integer.parseInt(req.getParameter("id"));
		String newAdminName = req.getParameter("admin name");
		String newAdminMail = req.getParameter("admin mail");
		String newPassword  = req.getParameter("password");
		System.out.println(" Id :" +id+ " newAdminName :" + newAdminName + " newAdminMail :" +newAdminMail + " newPassword :" + newPassword);
		CompanyDao cod = new CompanyDao();
		LoginDao ld = new LoginDao();
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		try {
			ld.updateUserPass(cod.getAdminMail(id),newAdminMail, newPassword); //first argument is old AdminMail
			if (cod.update(id, newAdminName, newAdminMail))
				pw.write("<h2>Updated successfully</h2>");
			else
				pw.write("<h2>Id does not exist..Please enter a valid Id</h2>");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			pw.write("<h2>Could not update..try again</h2>");
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			pw.write("<h2>Could not update..try again</h2>");
			e.printStackTrace();
		}
		finally {
			pw.close();		
		}
	}
}
