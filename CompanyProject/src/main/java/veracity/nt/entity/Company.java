package veracity.nt.entity;

public class Company {
	private int compId;
	private String compName;
	private String compLocation;
	private String adminName;
	private String adminMail;
	public Company(String compName, String compLocation, String adminName, String adminMail) {
		super();
		this.compName = compName;
		this.compLocation = compLocation;
		this.adminName = adminName;
		this.adminMail = adminMail;
	}
	public int getCompId() {
		return compId;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	public String getCompLocation() {
		return compLocation;
	}
	public void setCompLocation(String compLocation) {
		this.compLocation = compLocation;
	}
	public String getAdminName() {
		return adminName;
	}
	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
	public String getAdminMail() {
		return adminMail;
	}
	public void setAdminMail(String adminMail) {
		this.adminMail = adminMail;
	}
}
