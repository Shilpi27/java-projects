package veracity.nt.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDao {
	public void add(String username,String Password) throws ClassNotFoundException, SQLException {
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("Insert into Login values(?,?)");
			ps.setString(1, username);
			ps.setString(2, Password);
			int result = ps.executeUpdate();
			System.out.println(result + "record inserted in Login table - " + username + " : " + Password);
			conn.close();
			
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
	}
	public void updatePassword(String username,String password) throws ClassNotFoundException, SQLException {
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Login where username=?");
			pSelect.setString(1, username);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	System.out.println("Recieved from select " + rs.getString(1) + " : " + rs.getString(2));
		    	PreparedStatement ps = conn.prepareStatement("update Login set password=? where username=?");
		    	ps.setString(1, password);
		    	ps.setString(2, username);
		    	int result = ps.executeUpdate();
		    	System.out.println(result + "record updated in Login table");
		    }
		    else {
		    	System.out.println("No details found for username : " + username);
		    }
			conn.close();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
	}
	public void updateUserPass(String oldUsername,String newUsername,String newPassword ) throws ClassNotFoundException, SQLException {
		try 
		{
			System.out.println("Recieved oldUsername : " + oldUsername + " newUsername :" + newUsername + " newPassword :"+newPassword);
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Login where username=?");
			pSelect.setString(1, oldUsername);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	System.out.println("Recieved from select " + rs.getString(1) + " : " + rs.getString(2));
		    	PreparedStatement ps = conn.prepareStatement("update Login set username=?,password=? where username=?");
		    	ps.setString(1, newUsername);
		    	ps.setString(2, newPassword);
		    	ps.setString(3, oldUsername);
		    	int result = ps.executeUpdate();
		    	System.out.println(result + "record updated in Login table");
		    }
		    else {
		    	System.out.println("No details found for username : " + oldUsername);
		    }
			conn.close();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
	}
	public boolean validateLogin(String username,String password) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Login where username=? and password=?");
			pSelect.setString(1, username);
			pSelect.setString(2, password);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	System.out.println("Recieved from select " + rs.getString(1) + " : " + rs.getString(2));
		    	return true;
		    }
		    else {
		    	System.out.println("No details found for username : " + username + " password :" +password);
		    	return false;
		    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
		return false;
	}
	public void removeUser(String username) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			PreparedStatement pSelect = conn.prepareStatement("delete from Login where username=?");
			pSelect.setString(1, username);
		    int result = pSelect.executeUpdate();
		    System.out.println(result + " record deleted from Login table for username :" + username);
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
	}
}
