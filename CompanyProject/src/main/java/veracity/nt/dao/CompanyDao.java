package veracity.nt.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import veracity.nt.entity.Company;

public class CompanyDao {
	public void add(Company c) throws ClassNotFoundException, SQLException {
		try 
		{
			/*InputStream is = new FileInputStream("/CompanyProject/src/main/java/veracity/config/app.properties");
			Properties ps1 = new Properties();
			ps1.load(is);
			Class.forName("driver");
			Connection conn = DriverManager.getConnection(ps1.getProperty("url"),ps1.getProperty("username"),ps1.getProperty("password"));
			*/
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);

			System.out.println("Connection created " + conn);
			PreparedStatement ps = conn.prepareStatement("Insert into Company(compName, location, adminName, adminMail) values(?,?,?,?)");
			ps.setString(1, c.getCompName());
			ps.setString(2, c.getCompLocation());
			ps.setString(3, c.getAdminName());
			ps.setString(4, c.getAdminMail());
			int result = ps.executeUpdate();
			System.out.println(result + "record inserted in Company table");
			conn.close();
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
	}
	public boolean update(int id ,String adminName,String adminMail) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Company where compId=?");
			pSelect.setInt(1, id);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	System.out.println("Recieved from select " + rs.getString(1) + " : " + rs.getString(2));
		    	PreparedStatement ps = conn.prepareStatement("update Company set adminName=?,adminMail=? where compId=?");
		    	ps.setString(1, adminName);
		    	ps.setString(2, adminMail);
		    	ps.setInt(3, id);
		    	int result = ps.executeUpdate();
		    	System.out.println(result + "record updated in Company table");
		    	return true;
		    }
		    else {
		    	System.out.println("No details found for id : " + id);
		    	return false;
		    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
		return false;
	}
	public boolean remove(int id ) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
		    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Company where compId=?");
			pSelect.setInt(1, id);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	PreparedStatement ps = conn.prepareStatement("delete from Company where compId=?");
		    	ps.setInt(1, id);
		    	int result = ps.executeUpdate();
		    	System.out.println(result + "record deleted from Company table");
		    	return true;
		    }
		    else {
		    	System.out.println("No details found for id : " + id);
		    	return false;
		    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
		return false;
	}
	public String viewCompanyDetails(int id) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
		    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Company where compId=?");
			pSelect.setInt(1, id);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	System.out.println("Recieved from select " + rs.getString(1) + " : " + rs.getString(2));
		    	
		    	String tableData = "<table style='width:100%'><tr><th>ID</th><th>NAME</th><th>LOCATION</th><th>ADMIN NAME</th><th>ADMIN MAIL</th></tr>"; 
		    	tableData += "<tr><td>" + rs.getString(1) + "</td><td>" + rs.getString(2) + "</td><td>" + rs.getString(3) + "</td><td>" + rs.getString(4) + "</td><td>" + rs.getString(5) + "</td></tr></table>";
		    	
		    	return tableData;
		    }
		    else {
		    	System.out.println("No details found for id : " + id);
		    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
		return "null";
	}
	public String viewAllCompany( ) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		String tableData = null;
		try 
		{
			Properties prop = new Properties();
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");
			if (inputStream != null)
			{
				prop.load(inputStream);
			}
			else {
				System.out.println("File not found");
			}
			Class.forName("driver");
		    conn = DriverManager.getConnection(prop.getProperty("url"),prop.getProperty("username"),prop.getProperty("password"));
		    System.out.println("Connection created " + conn);
			/*Class.forName("com.mysql.jdbc.Driver");
		    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);*/
			PreparedStatement pSelect = conn.prepareStatement("select * from Company");
		    ResultSet rs = pSelect.executeQuery();	  
		    tableData = "<table style='width:100%'><tr><th>ID</th><th>NAME</th><th>LOCATION</th><th>ADMIN NAME</th><th>ADMIN MAIL</th></tr>"; 
		    while (rs.next()) {
		    	System.out.println("Recieved from select " + rs.getString(1) + " : " + rs.getString(2));
		    	tableData += "<tr><td>" + rs.getString(1) + "</td><td>" + rs.getString(2) + "</td><td>" + rs.getString(3) + "</td><td>" + rs.getString(4) + "</td><td>" + rs.getString(5) + "</td></tr>";
		    }
		    tableData += "</table>";
		    return tableData;
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
		return tableData;
	}
	public String getAdminMail(int id) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
		    conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/veracityassignment","root","Shilpi@7084");
			System.out.println("Connection created " + conn);
			PreparedStatement pSelect = conn.prepareStatement("select * from Company where compId=?");
			pSelect.setInt(1, id);
		    ResultSet rs = pSelect.executeQuery();
		    if (rs.next()) {
		    	System.out.println("Id :" + rs.getString(1) + " Admin Mail :" + rs.getString(5));
		    	return rs.getString(5);
		    }
		    else {
		    	System.out.println("No details found for id : " + id);
		    }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		finally {
			conn.close();
		}
		return "null";
	}
}
