package com.nt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nt.dao.UserDao;

@Service
public class UserService {

	@Autowired
	UserDao dao;
	
	public boolean validateLogin(String username, String password) {
		return dao.validateLogin(username, password);
	}

	public String[] viewMessagesByType(String username,String mesgType ) {
		return dao.viewMessagesByType(username,mesgType);
	}

}
