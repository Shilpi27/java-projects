package com.nt.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {
	@Autowired
	UserDao dao;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public boolean validateLogin(String username, String password) {
		Object args[] = {username,password};
		
		int count = jdbcTemplate.queryForObject("select count(*) from login where username=? and password=?", args ,new RowMapper<Integer>() {

			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getInt(1);
			}});
		if (count ==1)
			return true;
		return false;
	}

	public String[] viewMessagesByType(String username,String mesgType) {
		Object args[] = {username,mesgType};
		String messages = jdbcTemplate.queryForObject("select message from mail_mesg where username=? and messageType=?",args,new RowMapper<String>() {

			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString(1);
			}});
		String mesgList[] = messages.split(",");
		return mesgList;
	}
}
