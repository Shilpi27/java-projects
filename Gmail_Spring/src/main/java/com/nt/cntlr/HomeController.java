package com.nt.cntlr;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.nt.service.UserService;

@Controller
@SessionAttributes("user")
public class HomeController {

	@Autowired
	UserService service;
	
	@RequestMapping("/loginForm")
	public String loginForm() {
		return "login";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String login(@RequestParam("username") String username,@RequestParam("password") String password,Model model) {
		boolean isValid = service.validateLogin(username,password);
		if (!isValid) {
			model.addAttribute("error","Invalid Credentials..Try Again");
			return "login";
		}
		//session.setAttribute("user",username );
		model.addAttribute("user",username );
		return "gmailHome";
	}
	
	@RequestMapping("/inbox")
	public String inbox(HttpServletRequest req,Model model) {
		String username = (String) req.getSession().getAttribute("user");
		if (username == null) {
			model.addAttribute("error","Login first..");
			return "login";
		}
		String msg[] = service.viewMessagesByType(username,"Inbox");
		model.addAttribute("User",username);
		model.addAttribute("MesgType","Inbox :");
		model.addAttribute("msgList",msg);
		return "displayMessage";
	}
	
	@RequestMapping("/logout")
	//public String logout(HttpSession session,Model model) {
	public String logout(SessionStatus status) {
		/*if (session != null) {
			session.invalidate();
			model.addAttribute("error","Logout successfully..");
		}*/
		status.setComplete();
		return "login";
	}
}
